package com.darryring.libmodel.entity.theme;

/**
 * Created by hljdrl on 16/6/4.
 */
public class ThemeEntity {
    //==================================================
    public final static int TYPE_THEME = 0;
    public final static int TYPE_SPLASH_THEME = 1;
    public final static int TYPE_NO_ACTION_BAR = 2;
    public final static int TYPE_FRIENDS_NO_ACTION_BAR_THEME = 3;
    public final static int TYPE_BROWSER_NOACTION_BAR = 4;
    //==================================================
    private String themeId;
    private int theme;
    private int viewBackground;
    private int splashTheme;
    private int noActionBarTheme;
    private int friendsNoActionBarTheme;
    private int browsersNoActionBarTheme;
    //
    private int menu_item_textcolor;

    public String getThemeId() {
        return themeId;
    }

    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public int getViewBackground() {
        return viewBackground;
    }

    public void setViewBackground(int viewBackground) {
        this.viewBackground = viewBackground;
    }

    public int getSplashTheme() {
        return splashTheme;
    }

    public void setSplashTheme(int splashTheme) {
        this.splashTheme = splashTheme;
    }

    public int getNoActionBarTheme() {
        return noActionBarTheme;
    }

    public void setNoActionBarTheme(int noActionBarTheme) {
        this.noActionBarTheme = noActionBarTheme;
    }

    public int getFriendsNoActionBarTheme() {
        return friendsNoActionBarTheme;
    }

    public void setFriendsNoActionBarTheme(int friendsNoActionBarTheme) {
        this.friendsNoActionBarTheme = friendsNoActionBarTheme;
    }

    public int getBrowsersNoActionBarTheme() {
        return browsersNoActionBarTheme;
    }

    public void setBrowsersNoActionBarTheme(int browsersNoActionBarTheme) {
        this.browsersNoActionBarTheme = browsersNoActionBarTheme;
    }
}
