package com.darryring.fast.activitys;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.darryring.fast.R;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libcore.base.BaseAppCompatActivity;
import com.darryring.libmodel.entity.theme.ThemeEntity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ImagViewActivity extends BaseAppCompatActivity {

    private String TAG="ImagViewActivity";
    ImageView imageView;
    TextView mTextViev;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mTextViev = (TextView) findViewById(R.id.textView6);

        ColorStateList itemIconTint = getResources().getColorStateList(R.color.hellokitty_menu_item_textcolor);
        Drawable icon = getResources().getDrawable(R.drawable.ic_menu_exit);
        if (icon != null) {
            Drawable.ConstantState state = icon.getConstantState();
            icon = DrawableCompat.wrap(state == null ? icon : state.newDrawable()).mutate();
            icon.setBounds(0, 0, icon.getMinimumWidth(), icon.getMinimumHeight());
            DrawableCompat.setTintList(icon, itemIconTint);
        }
        TextViewCompat.setCompoundDrawablesRelative(mTextViev, icon, null, null, null);
        mTextViev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextViev.setSelected(!mTextViev.isSelected());
                Toast.makeText(ImagViewActivity.this,"click..",Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
