package com.darryring.fast.activitys;

import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.darryring.fast.R;
import com.darryring.fast.adapter.HomeAdapter;
import com.darryring.fast.adapter.data.TabEntity;
import com.darryring.fast.fragment.BaseFragment;
import com.darryring.fast.fragment.ContactFragment;
import com.darryring.fast.fragment.MenuListFragment;
import com.darryring.fast.fragment.SessionFragment;
import com.darryring.fast.fragment.UserFragment;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libmodel.entity.theme.ThemeEntity;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.flyco.tablayout.utils.UnreadMsgUtils;
import com.flyco.tablayout.widget.MsgView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FlycoTabHomeActivity extends AppCompatActivity {

    private String[] mTitles = {"聊天", "通信录", "功能", "账户"};

    private int menuIconIds[] = {com.darryring.libres.R.drawable.libres_item_chat_drawable,
            com.darryring.libres.R.drawable.libres_item_contact_drawable,
            com.darryring.libres.R.drawable.libres_item_app_drawable,
            com.darryring.libres.R.drawable.libres_item_preson_drawable};

    private int[] mIconUnselectIds = {
            R.drawable.iconfont_message_normal,
            R.drawable.iconfont_friends_normal,
            R.drawable.iconfont_app_normal,
            R.drawable.iconfont_person_normal};
    private int[] mIconSelectIds = {
            R.drawable.iconfont_message_pressed, R.drawable.iconfont_friends_pressed,
            R.drawable.iconfont_app_pressed, R.drawable.iconfont_person_pressed};


    CommonTabLayout mCommonTabLayout;
    ViewPager mViewPager;
    HomeAdapter mHomeAdapter;

    ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    ThemeEntity mThemeEntity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mThemeEntity = FastTheme.fastTheme.theme();
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flyco_tab_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mCommonTabLayout = (CommonTabLayout) findViewById(R.id.tabbar);
        //
        List<BaseFragment> fragments = new ArrayList<>();
        fragments.add(new SessionFragment("聊天"));
        fragments.add(new ContactFragment("通信录"));
        fragments.add(new MenuListFragment("功能"));
        fragments.add(new UserFragment("账户"));
        //
        //--------------------------------------------
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }
        //
        mHomeAdapter = new HomeAdapter(getSupportFragmentManager(),fragments);
        mViewPager.setAdapter(mHomeAdapter);
        mViewPager.setOffscreenPageLimit(fragments.size());
        //==========================================================

        //==========================================================
        setupTabBar();
        //两位数
        mCommonTabLayout.showMsg(0, 55);
        mCommonTabLayout.setMsgMargin(0, -5, 5);

        //三位数
        mCommonTabLayout.showMsg(1, 100);
        mCommonTabLayout.setMsgMargin(1, -5, 5);

        //设置未读消息红点
        mCommonTabLayout.showDot(2);
        MsgView rtv_2_2 = mCommonTabLayout.getMsgView(2);
        if (rtv_2_2 != null) {
            UnreadMsgUtils.setSize(rtv_2_2, dp2px(7.5f));
        }

        //设置未读消息背景
        mCommonTabLayout.showMsg(3, 5);
        mCommonTabLayout.setMsgMargin(3, 0, 5);
        MsgView rtv_2_3 = mCommonTabLayout.getMsgView(3);
        if (rtv_2_3 != null) {
            rtv_2_3.setBackgroundColor(Color.parseColor("#6D8FB0"));
        }
    }

    private void setupTabBar() {
        mCommonTabLayout.setTabData(mTabEntities);
        mCommonTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                    mViewPager.setCurrentItem(position, false);
            }

            @Override
            public void onTabReselect(int position) {
                if (position == 0) {
                    mCommonTabLayout.showMsg(0, mRandom.nextInt(100) + 1);
                    UnreadMsgUtils.show(mCommonTabLayout.getMsgView(0), mRandom.nextInt(100) + 1);
                }
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                mCommonTabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    Random mRandom = new Random();
    protected int dp2px(float dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
