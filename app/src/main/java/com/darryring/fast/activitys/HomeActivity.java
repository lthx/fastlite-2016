package com.darryring.fast.activitys;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.darryring.fast.R;
import com.darryring.fast.adapter.HomeAdapter;
import com.darryring.fast.compat.EventCompat;
import com.darryring.fast.fragment.BaseFragment;
import com.darryring.fast.fragment.ContactFragment;
import com.darryring.fast.fragment.MenuListFragment;
import com.darryring.fast.fragment.SessionFragment;
import com.darryring.fast.fragment.UserFragment;
import com.darryring.fast.theme.FastTheme;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.fast.util.MoviceSize;
import com.darryring.fast.util.UriForamt;
import com.darryring.libcore.Fast;
import com.darryring.libcore.base.BaseAppCompatActivity;
import com.darryring.libcore.core.MessageEvent;
import com.darryring.libmodel.entity.theme.ThemeEntity;
import com.darryring.libview.PagerTitleTabStrip;
import com.facebook.drawee.view.SimpleDraweeView;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;


public class HomeActivity extends BaseAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    ViewPager mViewPager;
    HomeAdapter mHomeAdapter;
    PagerTitleTabStrip tabs;
    private Rect mItemIconRect = new Rect(0,20,75,85);
    private SimpleDraweeView userImageView;
    ThemeEntity mThemeEntity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mThemeEntity = FastTheme.fastTheme.theme();
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_home);
        tabs = (PagerTitleTabStrip) findViewById(R.id.tabs);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View _headerView = navigationView.getHeaderView(0);
        if(_headerView!=null) {
            userImageView = (SimpleDraweeView) _headerView.findViewById(R.id.imageview_user);
        }
        MoviceSize _iconSize = MoviceSize.make(75,85);
        int  _textSize = MoviceSize.makeText(28,28);

        mItemIconRect = new Rect(0,20,_iconSize.width,_iconSize.height);

        MoviceSize _tabBarSize =  MoviceSize.makeHeight(168,168);
        //
        ViewGroup.LayoutParams _tabBarParams = tabs.getLayoutParams();
        if(_tabBarParams!=null){
            _tabBarParams.height = _tabBarSize.height;
        }

        List<BaseFragment> fragments = new ArrayList<>();
        fragments.add(new SessionFragment("聊天"));
        fragments.add(new ContactFragment("通信录"));
        fragments.add(new MenuListFragment("功能"));
        fragments.add(new UserFragment("账户"));
        mHomeAdapter = new HomeAdapter(getSupportFragmentManager(),fragments);
        mViewPager.setAdapter(mHomeAdapter);
        mViewPager.setOffscreenPageLimit(fragments.size());
        if(FastTheme.fastTheme.theme()!=null){
            TypedArray ta = getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
            int _defaultColor =getResources().getColor(R.color.libres_menu_text_normal);
            int _selectColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);

            ColorStateList colorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
//            tabs.setTabTextColorStateList(getResources().getColorStateList(_theme.getMenu_item_textcolor()));
            tabs.setTabTextColorStateList(colorStateList);
            //设置tab icon
            ColorStateList iconColorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
            tabs.setTabIconColorStateList(iconColorStateList);
            //
            ta.recycle();
        }else{
            tabs.setTabTextColorStateList(getResources().getColorStateList(R.color.libres_menu_item_textcolor));
        }

        tabs.setItemIconBounds(mItemIconRect);
        tabs.setTextSize(_textSize);
        tabs.setViewPager(mViewPager);
        //
        loadUserAvatorAsyn();
        EventCompat.postEvent(getClass().getSimpleName(),0);
    }

    @Override
    protected void callMessageEvent(MessageEvent event) {
        super.callMessageEvent(event);
        Toast.makeText(this, event.message, Toast.LENGTH_SHORT).show();
    }
    //    @Override
//    protected void onResume() {
//        super.onResume();
//        ThemeEntity temp = (ThemeEntity) Fast.fastTheme.theme();
//        if(temp!=mThemeEntity){
//            reload();
//        }
//    }

    private void loadUserAvatorAsyn(){
        MoviceSize _autosize = MoviceSize.makeHeight(200,200);
        String pkg = getPackageName();
        Uri _uri =   Uri.parse("res://"+pkg+"/"+R.drawable.ic_user_avator);
        userImageView.setImageURI(_uri);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    public void reload() {
//        Intent intent = getIntent();
//        overridePendingTransition(0, 0);//不设置进入退出动画
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        finish();
//        overridePendingTransition(0, 0);
//        startActivity(intent);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent mIm = new Intent(this,SettingsActivity.class);
            startActivity(mIm);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_image_selecter) {
            // Handle the camera action
            openImageSelecter();

        } else if (id == R.id.nav_gallery) {
            Intent mIm = new Intent(this,ImagePagerActivity.class);
            startActivity(mIm);
        }else if(id==R.id.nav_gallery_pading){
            Intent mIm = new Intent(this,ImagePagerPadingActivity.class);
            startActivity(mIm);
        }
        else if (id == R.id.nav_category) {
            Intent mIm = new Intent(this,CategoryActivity.class);
            startActivity(mIm);
        } else if (id == R.id.nav_qr_scan) {
            Intent mIm = new Intent(this,ScancodeActivity.class);
            startActivity(mIm);

        } else if (id == R.id.nav_qrcode) {
            Intent mIm = new Intent(this,QRCodeActivity.class);
            startActivity(mIm);
        }else if (id == R.id.nav_imagview) {
            Intent mIm = new Intent(this,ImagViewActivity.class);
            startActivity(mIm);
        }
        else if (id == R.id.nav_player) {
            Intent mIm = new Intent(this,VideoActivity.class);
            startActivity(mIm);
        } else if (id == R.id.nav_exit) {
            Fast.kvCache.deleteToFileCache(LoginActivity.SAVE_LOGIN_STATE);
            finish();
            MobclickAgent.onKillProcess(this);
        }else if(id == R.id.nav_error){
            throw new NullPointerException("I'm a cool exception and I crashed the main thread!");
        }else if(id == R.id.nav_friends_circle){
            Intent mIm = new Intent(this,FriendsCircleActivity.class);
            startActivity(mIm);
        }else if(id == R.id.nav_browser){
            Intent mIm = BrowserActivity.buildIntent(this,"http://www.hao123.com");
            startActivity(mIm);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    int REQUEST_IMAGE = 1000;
    void openImageSelecter(){
//        Intent intent = new Intent(this, com.libtrace.imageselector.LibImageSelectorActivity.class);
//
//        // 是否显示调用相机拍照
//        intent.putExtra(LibImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
//
//        // 最大图片选择数量
//        intent.putExtra(LibImageSelectorActivity.EXTRA_SELECT_COUNT, 9);
//
//        //显示预览按钮
//        intent.putExtra(LibImageSelectorActivity.EXTRA_SHOW_PREVIEW, true);
//
//        // 设置模式 (支持 单选/LibImageSelectorActivity.MODE_SINGLE 或者 多选/LibImageSelectorActivity.MODE_MULTI)
//        intent.putExtra(LibImageSelectorActivity.EXTRA_SELECT_MODE, LibImageSelectorActivity.MODE_MULTI);
//
//        startActivityForResult(intent, REQUEST_IMAGE);
        FilePickerBuilder.getInstance().setMaxCount(9)
                .setSelectedFiles(photoPaths)
                .setActivityTheme(mThemeEntity.getTheme())
                .pickPhoto(this);

    }
    private ArrayList<String> photoPaths = new ArrayList<>();
}
