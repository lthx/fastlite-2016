package com.darryring.fast.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.libcore.base.BaseAppCompatActivity;
import com.umeng.analytics.MobclickAgent;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;

/**
 * 应用程序错误异常页面,显示错误日志,重启APP
 */
public class ErrorCrashActivity extends BaseAppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_crash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //================================================
        TextView errorDetailsText = (TextView) findViewById(R.id.error_details);
        String errorText = CustomActivityOnCrash.getStackTraceFromIntent(getIntent());
        errorDetailsText.setText(errorText);

        Button restartButton = (Button) findViewById(R.id.restart_button);

        final Class<? extends Activity> restartActivityClass = CustomActivityOnCrash.getRestartActivityClassFromIntent(getIntent());
        final CustomActivityOnCrash.EventListener eventListener = CustomActivityOnCrash.getEventListenerFromIntent(getIntent());

        if (restartActivityClass != null) {
            restartButton.setText(R.string.restart_app);
            restartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ErrorCrashActivity.this, restartActivityClass);
                    CustomActivityOnCrash.restartApplicationWithIntent(ErrorCrashActivity.this, intent, eventListener);
                }
            });
        } else {
            restartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomActivityOnCrash.closeApplication(ErrorCrashActivity.this, eventListener);
                }
            });
        }
        MobclickAgent.reportError(this,errorText);
        MobclickAgent.onKillProcess(this);
    }
}
