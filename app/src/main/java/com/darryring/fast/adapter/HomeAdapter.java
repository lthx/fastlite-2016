package com.darryring.fast.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.darryring.fast.fragment.BaseFragment;
import com.darryring.libview.PagerTitleTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/3/2.
 */
public class HomeAdapter extends FragmentPagerAdapter implements PagerTitleTabStrip.IconTabProvider {

    private List<BaseFragment> mList = new ArrayList<>();

    public HomeAdapter(FragmentManager fm, List<BaseFragment> _list) {
        super(fm);
        mList.addAll(_list);
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mList.get(position).getTitle();
    }

    private int menuIconIds[] = {com.darryring.libres.R.drawable.libres_item_chat_drawable,
            com.darryring.libres.R.drawable.libres_item_contact_drawable,
            com.darryring.libres.R.drawable.libres_item_app_drawable,
            com.darryring.libres.R.drawable.libres_item_preson_drawable};

    @Override
    public int getPageIconResId(int position) {
        return menuIconIds[position];
    }
}
