package com.darryring.fast.util;

/**
 *  ImageLoader Uri Foramt
 *  "http://site.com/image.png" // from Web
 *  "file:///mnt/sdcard/image.png" // from SD card
 *  "file:///mnt/sdcard/video.mp4" // from SD card (video thumbnail)
 *  "content://media/external/images/media/13" // from content provider
 *  "content://media/external/video/media/13" // from content provider (video thumbnail)
 *  "assets://image.png" // from assets
 *  "drawable://" + R.drawable.img // from drawables (non-9patch images)
 *  Created by LiaoJunHui on 2015/11/26.
 */
public final class UriForamt {


    /**
     *  "file:///mnt/sdcard/image.png" // from SD card
     * 格式化File路径,ImageLoader要求格式
     * @param file
     * @return
     */
    public static String foramtUriFile(String file){
        StringBuffer _buf = new StringBuffer();
        _buf.append("file://").append(file);
        return _buf.toString();
    }

    /**
     * "http://site.com/image.png" // from Web
     * @param http
     * @return
     */
    public static String formatUriHttp(String http){
        return String.valueOf(http);
    }

    /**
     * "assets://image.png"
     * @param assets
     * @return
     */
    public static String formatUriAssets(String assets){
        StringBuffer _buf = new StringBuffer();
        _buf.append("assets://").append(assets);
        return _buf.toString();
    }

    /**
     * "drawable://" + R.drawable.img // from drawables (non-9patch images)
     * @param drawable
     * @return
     */
    public static String formatUriDrawable(int drawable){
        StringBuffer _buf = new StringBuffer();
        _buf.append("drawable://").append(drawable);
        return _buf.toString();
    }
}
