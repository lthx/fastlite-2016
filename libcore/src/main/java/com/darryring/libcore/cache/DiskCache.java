package com.darryring.libcore.cache;

/**
 *
 * Android DiskLruCache
 * Created by hljdrl on 16/3/5.
 */
public interface DiskCache {


    public void saveString(String key, String data);

    public String readString(String key);

    public boolean remove(String key);

    public long size();

}
