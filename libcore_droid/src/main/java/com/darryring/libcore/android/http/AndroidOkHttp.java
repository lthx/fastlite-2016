package com.darryring.libcore.android.http;

import android.content.Context;

import com.darryring.libcore.http.Http;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by hljdrl on 16/3/2.
 */
public class AndroidOkHttp implements Http {

    private String TAG="AndroidOkHttp";

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public static final MediaType MEDIA_TYPE_MARKDOWN
            = MediaType.parse("text/x-markdown; charset=utf-8");


    OkHttpClient client ;
    Context mContext;

    /**
     * @param ctx
     */
    public AndroidOkHttp(Context ctx)
    {
        mContext = ctx;
        client = new OkHttpClient.Builder()
                .build();
    }


    /**
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    @Override
    public String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body).addHeader("charset","UTF-8")
                .addHeader("Content-Type","application/json")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    /**
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    @Override
    public String postJSON(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .post(body).addHeader("charset","UTF-8")
                .addHeader("Content-Type","application/json")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    /**
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    @Override
    public byte[] postJSON2(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body).addHeader("charset","UTF-8")
                .addHeader("Content-Type","application/json")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().bytes();
    }

    /**
     *
     */
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    /**
     * @param url
     * @param file
     * @return
     * @throws Exception
     */
    @Override
    public String postFile(String url, File file) throws Exception {


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM).addFormDataPart("uploadfile", file.getName(),RequestBody.create(MEDIA_TYPE_PNG,file))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();


        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        String _reuslt = response.body().string();
        return _reuslt;
    }

    /**
     * @param url
     * @param string
     * @return
     * @throws Exception
     */
    @Override
    public String postString(String url, String string) throws Exception {
        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, string))
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);


        String _result = response.body().string();
        System.out.println(_result);
        return _result;
    }

    /**
     * @param url
     * @return
     * @throws IOException
     */
    @Override
    public String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
