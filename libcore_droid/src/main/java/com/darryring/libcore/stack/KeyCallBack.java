package com.darryring.libcore.stack;

import android.view.KeyEvent;

/**
 */
public interface KeyCallBack {
    boolean onKeyDown(int keyCode, KeyEvent event);
}
