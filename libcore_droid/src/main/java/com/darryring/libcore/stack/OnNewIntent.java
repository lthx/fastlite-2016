package com.darryring.libcore.stack;

/**
 */
public interface OnNewIntent {
    /**
     * SingleTop mode,this fragment not be Re create
     */
    void onNewIntent();
}
