package com.darryring.libchat;

import com.darryring.libchat.call.OkCall;
import com.darryring.libchat.listener.FastChatListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/4/11.
 */
public abstract class FastChat<T> {

    private static FastChat<?> mInstance = null;

    public synchronized static final FastChat<?> getInstance(Class<? extends FastChat> _class){
        if(mInstance==null){
            try {
                mInstance = _class.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return mInstance;
    }

    private List<FastChatListener> mFastChatListeners = new ArrayList<FastChatListener>();

    /**
     * @param _call
     */
    public void addFastChatListener(FastChatListener _call){
        if(_call!=null){
            if(!mFastChatListeners.contains(_call)){
                mFastChatListeners.add(_call);
            }
        }
    }

    /**
     * @param _call
     */
    public void removeFastChatListener(FastChatListener _call){
        if(_call!=null){
            if(mFastChatListeners.contains(_call)){
                mFastChatListeners.remove(_call);
            }
        }
    }
    protected  void notifyFastChatListeners(int typeCall){
        List<FastChatListener> _copy = new ArrayList<>(mFastChatListeners);
        for(FastChatListener _call:_copy){
            _call.callChat(typeCall);
        }
    }
    protected abstract void postRunnable(Runnable _runnable);

    /**
     * @param t
     */
   public abstract void init(T t);

    /**
     * @param k
     * @param v
     */
   protected abstract void setConfig(String k, String v);

    /**
     * @param k
     * @return
     */
    protected abstract  String getConfig(String k);

   public abstract boolean isOnline();

    /**
     * @param user
     * @param pwd
     */
    public abstract void login(String user,String pwd);

    /**
     * @param user
     * @param pwd
     * @param _call
     */
    public abstract void loginAsyn(String user,String pwd,OkCall _call);
    /**
     *
     */
    public abstract void logout();

    /**
     * @param _call
     */
    public abstract  void logout(OkCall _call);

    /**
     * @return
     */
    public abstract ContactManager contactManager();

    /**
     * @return
     */
    public abstract ChatManager chatManager();
}
